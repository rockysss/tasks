##Cloud Service Provider:-

### Google Cloud:
Google Cloud Platform is essentially a public cloud-based machine whose services are delivered to customers on an as-you-go basis, by way of service components.
The targeted core user for GCP at present appears to be the business -- small, medium, or large -- that is well into its journey to modern application models, and needs a more cost-effective and efficient means of deploying them.
GCP's strategy for competing on price is to offer discounts for sustained use, for customized use, and for committed use.

#### Features:
* Automating the deployment of modern applications.
* Creative cost control. 
* Friendlier hand-holding for first-time users.

### Oracle Cloud:

Oracle Cloud is a cloud computing service that combines Infrastructure as a Service (IaaS), Software as a Service (SaaS), Platform as a Service (PaaS), and Data as a Service (DaaS) solutions within Oracle's cloud infrastructure.
Oracle Cloud is meant to deliver business users and developers with seamless workloads that meet server, storage, application, and network needs.

####Features of oracle cloud:
    
* Oracle offers customizable Virtual Cloud Networks (VCN), IP addresses, routing, and firewalls to support private networks with continuous security.
* With this service users get Real Application Cluster (RAC) reliability, data security, and granular controls all in one reliable cloud environment.
* Oracle provides customers with Virtual Machine instances that are made for varying levels of workloads and business performance.
* Oracle's IaaS offers block volume, archive storage, and object storage to suit application needs that come from small websites to larger more demanding enterprise applications.

### AWS:
Amazon Web Services (AWS) is a cloud service provider offering Infrastructure as Service (IaaS) and Platform as a Service (PaaS) solutions. Amazon's cloud service offering provides users with server, networking, storage, remote computing, email, and security solutions. AWS is one of the biggest vendors in the cloud computing market, hosting prominent websites like Netflix and Instagram.

####Features Of AWS:-
* This solution is used for long-term archiving. It ensures that deep storage backup items are secure and safe, even though they are rarely used.
* Amazon's Elastic Compute Cloud (EC2) provides users with resizable compute capacity so that users can launch as many virtual private servers as needed while maintaining security and networking features.
* This platform allows users to set up several AWS services on the go with common language capability.

## Comparison between AWS and oracle cloud :-
    
##### On the basis of cost and payment:-

###### AWS :- AWS offers a pay-as-you-go fee model giving customers the control over how much services to add as they grow

###### Oracle Cloud :- Oracle Cloud also offers a pay-as-you-go model where laaS and PaaS services are metered hourly and charged only for the resource consumed, according to their website.

##### On the basis of market place:- 

###### AWS :- Oracle Cloud entered the market in 2015. Oracle had a very robust set of product offerings such as Oracle Enterprise Resource Planning, Oracle Enterprise Performance Management and more. The company established their expert presence in the field of software and software licensing solutions.

###### Oracle Cloud :- Amazon established and hosts some of the largest and notable public clouds like SAP Hana's cloud database, Netflix, and Instagram. The company entered the cloud computing market back in 2006, establishing multiple data centers across the world

##### On the basis of customer support:-

###### AWS :- The AWS Partner Network (APN) provides AWS-based businesses with technical, marketing, and go-to-market support. They give partners the resources and support to advance specific business needs.

###### Oracle Cloud :- The Oracle Partner Network (OPN) is very similar whereas it provides businesses with the actionable ability to make the move to cloud-based services. 

#### DigitalOcean:
The Oracle Partner Network (OPN) is very similar whereas it provides businesses with the actionable ability to make the move to cloud-based services. Digitalocean's droplet is a scalable computer service. It is more than just virtual machines. This cloud platform offers add-on storage, security, and monitoring capabilities to run production applications easily.


#### Features :- 
* Aesthetic and No-Fuss User Interface
* Remarkable Performance
* Professional Documentation
* Affordable Pricing

#### Alibaba Cloud: 
Alibaba Cloud develops highly scalable cloud computing and data management services. As the cloud computing arm and business unit of Alibaba Group, It provides a comprehensive suite of global cloud computing services to power both our international customers’ online businesses and Alibaba Group’s own e-commerce ecosystem. Alibaba Cloud’s international operations are registered and headquartered in Singapore, and the company has international teams stationed in Dubai, Frankfurt, Hong Kong, London, New York, Paris, San Mateo, Seoul, Singapore, Sydney, and Tokyo.


##### Microsoft Azure
Azure is a cloud computing platform which is launched by Microsoft in February 2010. This open source and flexible cloud platform which helps in development, data storage, service management & hosting solutions.

###### Features:
* Windows Azure offers the most effective solution for your data needs
* Provides scalability, flexibility, and cost-effectiveness
* Offers consistency across clouds with familiar tools and resources
* Allow you to scale your IT resources up and down according to your business needs








