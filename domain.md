### Web Hosting Companies:- 

#### GoDaddy :-
    The grand poobah of web hosting companies, GoDaddy has dominated the industry for more than 20 years. Seemingly 
    everyone’s first web host and domain registrar, GoDaddy boasts more than 77 million domain names under management 
    and 8,000 employees around the world. GoDaddy, which also owns managed hosting leader Media Temple and the 
    popular Host Europe Group, specializes in affordable and user-friendly shared hosting and domain registrations.

    Users can find specialized options that cater to website builders, eCommerce stores, WordPress users, and site owners  needing more powerful virtualized or dedicated servers. When you add in the vast array of SSL certificates, web security, online marketing, and email tools, GoDaddy surely has something for everyone.

    Known for: Low-cost domains and hosting
    Price: $1.00–$89.99 per month
    Domains: FREE first year (then $14.99)
    Email: FREE for the first year
    eCommerce: Yes
    Website builder: Yes

#### Bluehost: 
    Bluehost is one of the most commonly used web hosts out there today. A lot of hosting companies are a dime a 
    dozen. What you need is a hosting provider that places a focus on reliability, uptime, performance, and is 
    equipped with the necessary features to help you build and grow your site.
    Bluehost is a company that’s been in high demand since their inception over 15 years ago. They’ve been 
    effectively serving customers across the globe by providing an affordable, reliable, and high-performing service.

    Price: $2.75 per month

### DreamHost:
    Hosting over 1.5 million websites, DreamHost offers good performance at a good price >but has no live support 
    option. The cheapest plan starts at $2.59/mo (with a 36-month commitment) and renews at $4.95/mo. You can host 1 
    website and the plan comes with >the unlimited storage and bandwidth, free SSL and a generous 97-day money-back 
    guarantee.

    Price: $2.59 per month

### GreenGeeks:
    GreenGeeks is an environmentally friendly web hosting company with good performance. >The cheapest plan starts at 
    $2.95/mo (with a 36-month commitment) and renews at $9.95/>month. You can host 1 website and the plan includes 
    unlimited storage and bandwidth, >free SSL and a 30-day money-back guarantee.

    Price: $2.95 per month

#### HostGator:
    HostGator offers unlimited storage and bandwidth and good support to assist you. The cheapest plan starts at $2.99/month (with a 6-month commitment) and renews at $14.95/month or $3.98/month (with a 36-month commitment) renewing at $9.95/month. You can host >1 website and thee plan includes generous unmetered storage and bandwidth, free SSL >and a solid 45-day money-back guarantee.

    Price: $2.99-$14.95 per month

#### A2Hosting :-

    A2Hosting has the fastest page loading speed from the reviewed service providers. The cheapest plan starts at 
    $2.96/mo (with a 24-month commitment) and renews at $7.99/ per month.
    You can host 1 website and the plan comes with unlimited storage, bandwidth and free SSL. To make things even 
    better, they offer a risk-free anytime money-back guarantee.

    Price: $2.96 per month

#### AWS :-
    There should be no surprise that the company leading the charge in one of the industry’s hottest technologies 
    ranks high on the list of largest hosting providers. Cloud-hosting behemoth Amazon Web Services extends beyond 
    simple web hosting to power more than 90 services, including cloud computing, storage, networking, database, 
    analytics, application services, deployment, management, mobile, developer tools, and systems for the Internet of 
    Things.

    The on-demand cloud platform began as an in-house innovation retail giant Amazon re-launched in 2006 after 
    typically be reserved for experienced developers and established, larger businesses.
    associated with  AWS, combined with the lack of support commonly found among traditional web hosts, these cloud 
    services should typically be reserved for experienced developers and established, larger businesses.

    Price: Pay as you go
    Domains: $12.00 per year
    Email: Yes (Amazon SES)
    eCommerce: No
    Website builder: No 

    