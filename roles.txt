Different Roles in IT :-

Business Analyst:-

   Business Analyst is in charge of collating business requirements, writing functional specifications and the like -
   they define (or help define) the features of the software, the functional scope of projects, etc. 
   They are often involves in reviewing test plans to make sure that the functional changes they have listed are 
   tested thoroughly and correctly.
   Business analysts are the link between business people and IT teams in an organization. 
   They leverage data, processes and information to solve business problems and ensure IT projects run smoothly.

-------------------------------------------------------------------

Data Analyst:- 

A data analyst collects and stores data on sales numbers, market research, logistics, linguistics, or other 
behaviors. They bring technical expertise to ensure the quality and accuracy of that data, then process, design
and present it in ways to help people, businesses, and organizations make better decisions.

Data Analyst Responsibilities:
   * Commissioning and decommissioning of data sets.
   * Managing master data, including creation, updates, and deletion.
   * Managing users and user roles.
   * Provide quality assurance of imported data, working with quality assurance analys
   * Supporting the data warehouse in identifying and revising reporting requirements.

--------------------------------------------------------------------

Database Administrator:- 

   A Database Administrator is a person or a group of person who are responsible for managing all the activities
   related to database system. This job requires a high level of expertise by a person or group of person. 
   There are very rare chances that only a single person can manage all the database system activities so 
   companies always have a group of people who take care of database system.

   DBA is responsible for installing the database software. He configure the software of database and then 
   upgrades it if needed. There are many database software like oracle, Microsoft SQL and MySQL in the industry
   so DBA decides how the installing and configuring of these database software will take place.

--------------------------------------------------------------------

Quality Analyst:-

   QA is also known as a Quality Assurance Analyst, a Quality Analyst evaluates products, systems and 
   software to ensure they are free of defects and meet the quality standards of the organization.

   Quality Analyst Responsibilities:
   
      * Develop and execute test plans to ensure that all objectives are met.
      * Implement and monitor test scripts to assess functionality, reliability, performance, and quality of 
        the service or product.
      * Identify and remedy defects within the production process.
      * Recommend, implement, and monitor preventative and corrective actions to ensure that quality assurance standards are achieved.
         Compile and analyze statistical data.
      *  Ensure that user expectations are met during the testing process.
   
 ---------------------------------------------------------------------

Scrum master:-
   The scrum master is the team role responsible for ensuring the team lives agile values and principles
   and follows the processes and practices that the team agreed they would use.

   The responsibilities of this role include:
     
      *   Clearing obstacles
      *   Establishing an environment where the team can be effective
      *   Addressing team dynamics
      *   Ensuring a good relationship between the team and product owner as well as others outside the team
      *   Protecting the team from outside interruptions and distractions.

------------------------------------------------------------------------

Software Testers:-
   Software tester are responsible for the quality of software development and deployment. 
   They are involved in performing automated and manual tests to ensure the software created by 
   developers is fit for purpose. Some of the duties include analysis of software, and systems,
   mitigate risk and prevent software issues.

   A Software Tester is responsible for designing testing scenarios for usability testing.
   He is responsible for conducting the testing, thereafter analyze the results and then submit
   his observations to the development team. He may have to interact with the clients to better 
   understand the product requirements or in case the design requires any kind of modifications.
   Software Testers are often responsible for creating test-product documentation and
   also has to participate in testing related walk through.

-------------------------------------------------------------------------

DevOps Engineer:-

   DevOps Engineer is one of the most challenging roles and often organizations find it difficult 
   to find an efficient DevOps engineer. A DevOps engineer must have a strong passion for scripting and
   coding, has expertise in handling deployment automation, infrastructure automation and ability to handle
   the version control. 

   DevOps Engineer is responsible for handling the IT infrastructure as per the business needs of the code
   which can be deployed in hybrid multi-tenant environment which needs continuous monitoring of the
   performance. DevOps engineer must be aware of the development tools which write the new code or enhance 
   the existing code.   

--------------------------------------------------------------------------

UX stands for User Experience:- 

   UX design is a still a relatively new field, with many companies only just waking up to the fact that they
   need someone on their payroll if they want to succeed in attracting and retaining customers.
   Knowing who the target customers are, and how to make their experience with your product the most 
   rewarding or ‘delightful’ it can be, is the responsibility of the UX design team. As such, ‘functionality’,
   ‘usability’ and ‘user adaptability’ ranks high in their priorities for the product.
   UX designers are generally focused on development of digital products

-----------------------------------------------------------------------------

UI is known User Interface

   The more seamless the UI, the more intuitive the product feels.

   UI Designer’s job includes the following:

   Look and Feel:
   Customer Analysis
   Design Research
   Branding and Graphic Development
   User Guides/Storyline
   Responsiveness and Interactivity:
   UI Prototyping
   Interactivity and Animation
   Adaptation to All Device Screen Sizes
   Implementation with Developer
   A user interface designer makes technology easy and intuitive for people to use. User interface designers
   work on the areas where users directly interact with the product.

---------------------------------------------------------------------------------
Release Engineer:-

   A Release Engineer is an individual who is concerned with the mechanics of the development and processing 
   of software products. Release engineering, a sub-spec in software engineering, deals with the accumulation 
   and delivery of source codes into software or programs. These engineers oversee and control the proper placement 
   and deployment of the source code; they ensure that each code is entered in the software code repository and is 
   ready for media duplication and distribution.

   When it comes to software production, modern release engineers focus on the following aspects:-

      Consistency:      They need to offer a steady framework for development, audit, accountability and delivery
                        for various software components.
      Identifiability: They can distinguish all the required components for product releases.
      Reproducibility: They need to have the ability to guarantee the stability of operations by integrating sources 
                        and data and delivering what's essential to a program or software system.
      Agility:          They need to constantly research the advantages and consequences of modern software engineering
                        techniques and their implications for the software cycle.

---------------------------------------------------

 Business Requirement Document:-

   To arrive at a consensus with stakeholders
   To provide input into the next phase of the project
   To explain how customer/business needs will be met with the solution
   Holistic approach to business needs with the help of strategy that will provide some value to the customer

--------------------------------------------------------------------

Functional Requirement Document :-
   Few companies did not create FRD, instead they used BRD as it is detailed enough to be used as FRD as well
   FRD is derived from the BRD

   Most common objectives of FRD -

   Depict each process flows for each activity interlinking dependencies
   Holistic view for each and every requirements, steps to built
   Estimating detailed risks for each new change request
   Highlight the consequences if the project health is weak to avoid scope creep
   The FRD should document the operations and activities that a system must be able to perform.
