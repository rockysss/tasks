### Software Stack:
    


    LAMP(Linux/Apache/MySql/Php):
        
        LAMP stack, made up of all free, open-source software
        components that work especially well for dynamic web sites 
        and applications. It includes: the Linux operating system,
        the Apache web server, PHP application software, and MySQL
        database. It’s the most traditional stack model, and incredibly 
        solid.

    MEAN(MongoDB/Electron JS/Angular Js/Node Js)

        MEAN is a more modern stack challenging the LAMP stack.
        It’s entirely JavaScript-powered, too,so that makes it 
        a time and money-saver for companies that already have 
        JavaScript pros writing client-side code. It includes: 
        the MongoDB database,
        the leading JSON-powered NoSQL database that offers more
        flexibility than a relational SQL database. 
        The AngularJS front-end framework, Express.js, a web 
        framework for Node.js; and a base platform of the Node.js
        runtime, instead of an operating system. It offers 
        flexibility and lots of features for building single
        and multi-page web applications.

    MERN(MongoDB/Electron JS/React JS/Node Js)
        The term MEAN stack refers to a collection of JavaScript
        based technologies used to develop web applications. 
        MEAN is an acronym for MongoDB, ExpressJS, AngularJS and
        Node.js. From client to server to database, 
        MEAN is full stack JavaScript.

    WAMP(Window/Apache/MySql/Php)
        Windows which consists of Apache, MySQL and PHP. AMPPS 
        is a WAMP stack which can be installed on Desktop or on a
        Windows Server. Apache is the web server, MySQL is the
        database and PHP is server side scripting language.
        This WAMP Stack ships with PHP, PERL and Python server 
        side scripting language, PERL and Python runs on the CGI
        and mod_wsgi respectively on the Web server. Along with
        MySQL, AMPPS also provides the developers to work with 
        MongoDB Database. To manage the databases and tables, 
        phpMyAdmin for MySQL and RockMongo for MongoDB are provided.
