##Tools



--> Difference between git pull & fetch ?
==>	The fetch command retrieves any commits, references (like tags), 
branches and files from a remote repository, along with any other corresponding objects.
However, not all tags are retrieved as this command only takes the ones that point to commits that you are retrieving.
Basically this command fetches anything needed to reconstruct the history of the particular branch you're interested in.


The interesting thing about the fetch command is that it doesn't actually affect anything in your local repo.
No working changes will be lost, and you'll see no direct affect on your local branches. 
This is because Git keeps fetched content separate from your own repo's content until it is merged in.

https://stackabuse.com/git-difference-between-git-fetch-and-git-pull/
