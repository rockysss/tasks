## Frontend Technologies:-

It a set of technologies that are used in developing the user interface of web applications and webpages. With the help of front end technologies, the developers create the design, structure, animation, behavior and everything that you see on the screen while you open a web application, website or mobile app.

###React.js:

    ReactJS is a JavaScript library that combines the speed of JavaScript and uses a new way of 
    rendering webpages,making them highly dynamic and responsive to user input. 
    The product significantly changed the Facebook approach to development. 
    After the library was released as an open-source JavaScript tool in 2013,
    it became extremely popular due to its revolutionary approach to programming 
    user interfaces setting the long-going competition between React and Angular,
    another popular web development tool.

    Pros:
    * Virtual DOM in ReactJS makes user experience better and developer’s work faster
    * Permission to reuse React components significantly saves time
    * An open-source Facebook library: constantly developing and open to the community

    Cons:
    * poor state management 
    * doesnot support multirouting
    * highly dependent on third party libraries.


###Angular.js
    Angular is framework used to develop desktop, mobile as well as web application.

    Pros:
    * provide support to mvc pattern.
    * provide data model binding.
    * unit testing ready(karma)

    Cons:
    * slow loading and heavy bundle for loading
    * bad performance 



###Vue.js:
    Vue.js is a JavaScript library for developing distinct web interfaces. Its core library focuses on the view layer 
    only. Therefore, you can conveniently integrate it with other libraries and tools to achieve desired outputs. Also, 
    it is capable of powering Single Page Applications when you merge it with other tools and libraries.


    Pros:
        * Lightweight and small
        * Simple Integration and flexibile
        * Customization
        * support type scripting

    Cons:
        * lack of support
        * language barrier because it is written in chinese 
        * limited plugins



## Backend Technologies:

Backend programming can either be Object Oriented (OOP) or Functional.
The former is the technique that focuses on the creation of objects. With object-oriented programming, statements should be executed in a particular order. Popular OOP languages are Java, .NET, and Python.

###Java:
    Java is a programming language based on object oriented methodology.
    Java platform is a collection of programs that help to develop and run programs written in the Java programming language. Java platform includes an execution engine, a compiler, and a set of libraries. JAVA is platform-independent language. It is not specific to any processor or operating system.

    Pros:
    * Platform independent, simple and secure
    * provide good emory allocation
    * provide better and enhanced multithreding 

    Cons:
    * Java is slow in performance wise.
    * garbage collector affects the performance of java application.
    * java is strictly bounded.

###Python:
    Python is an interpreted, object-oriented,high-level programming language with dynamic semantics.Its high-level built in data structures, 
    combined with dynamic typing and dynamic binding, make it very attractive for
     Rapid Application Development, as well as for use as a scripting or glue 
     language to connect existing components 
     together. Python's simple, easy to learn syntax emphasizes readability and 
     therefore reduces the cost of program maintenance. Python supports modules and packages, which encourages program modularity and code reuse. The Python interpreter and the extensive standard library are available in source or binary form without charge for all major platforms, and can be freely distributed.

    pros:
        * Supports multiple systems and platforms
        * Gives rise to quick development by using less code.
        * Allows to scale even the most complex applications with ease.

    cons:
        * python is slow.
        * python is not a good choice for memory intensive tasks.
        * Has limitations with database access.
        * Python is not good for multi-processor/multi-core work.

###Django:
    Django is a high-level Python Web framework that encourages
    rapid development and clean, pragmatic design.
    Built by experienced developers, it takes care of much of the 
    hassle of Web development, so you can focus on writing your
    app without needing to reinvent the wheel. 
    It’s free and open source.

    pros:
      * Because it is implemented in python so it is very easy to read.
      * Better CDN(cotent delivery network) connectivity and Content Management
      * fast processing

    cons:
        * Django is monolithic.
        * Not for smaller projects
        * Uses Regular Expression for URLs

###Spring:
    Spring is open source, java based framework used to develop 
    all types of technologies and support IOC. Spring provide 
    flexibility to integrate with the ORM framework and also 
    with stratus and ejb.

    pros:
        * light weight beacause it uses of pojo classes.
        * No need of heavy weight server.
        * support AOP.
        * support modularity by providing seperate modules for every task.

    cons:
        * High Learning Curve
        * Lots of xml in spring but from jdk 1.5v it also support annotation



