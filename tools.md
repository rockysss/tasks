### List Of tools :-

#### Bitbucket:- 
Bitbucket is a tool used for Git repository management solution designed for professional teams. It gives you a central place to manage git repositories, collaborate on your source code and guide you through the development flow. 

#### Eclipse:-
Eclipse is an open source tool, famous for  Java Integrated Development Environment (IDE). It is written mostly in Java so as expected it is mostly used for JAVA SE and JAVA EE applications but it may also be used to develop applications in other programming languages via plug-ins, including: Ada , C , C++, Python, Ruby (including Ruby on Rails framework), etc. The Eclipse software development kit (SDK), which includes the Java development tools initially.

### Intellij:-
IntelliJ IDEA is an integrated development environment (IDE) written in Java for developing computer software. It is developed by JetBrains (formerly known as IntelliJ), and is available as an Apache 2 Licensed community edition, and in a proprietary commercial edition. Both can be used for commercial development

### Pycharm:-
PyCharm is cross-platform, with Windows, macOS and Linux versions. PyCharm is an integrated development environment (IDE) used in computer programming, specifically for the Python language. It is developed by the Czech company JetBrains. It provides code analysis, a graphical debugger, an integrated unit tester, integration with version control systems (VCSes), and supports web development with Django as well as Data Science with Anaconda.

### Atom:-
Atom is a free and open-source text and source code editor for macOS, Linux, and Microsoft Windows with support for plug-ins written in Node.js, and embedded Git Control, developed by GitHub. Atom is a desktop application built using web technologies. Most of the extending packages have free software licenses and are community-built and maintained. Atom is based on Electron (formerly known as Atom Shell), a framework that enables cross-platform desktop applications using Chromium and Node.js.

### VS code:- 
Visual Studio Code is a source-code editor developed by Microsoft for Windows, Linux and macOS. It includes support for debugging, embedded Git control and GitHub, syntax highlighting, intelligent code completion, snippets, and code refactoring. It is highly customizable, allowing users to change the theme, keyboard shortcuts, preferences, and install extensions that add additional functionality. The source code is free and open source and released under the permissive MIT License. The compiled binaries are freeware and free for private or commercial use.




