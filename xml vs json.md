### XML vs JSON:- 

XML :- It is Extensible markup language.       
JSON :- It is JavaScript Object Notation.

XML :- It is complex to read and understand.
JSON :- It is human understable and easy to read and write.

XML :- It is derived from SGML.
JSON:-It is based on JavaScript language.	

XML :- It is a markup language and uses tag structure to represent data items.
JSON:- It is a way of representing objects.	
 
XML :- It supports namespaces through XSD.
JSON :- It does not provides any support for namespaces.

XML :- It doesn’t supports array.
JSON:- It supports array.	

XML :- It uses dtd or xsd file to provide extensibility to create custom tag.
JSON :- It doesnot need any external file.



